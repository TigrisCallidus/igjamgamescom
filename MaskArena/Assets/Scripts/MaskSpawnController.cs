﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MaskSpawnController : MonoBehaviour {

    public GameObject spotLight;
    public Mask[] maskPrefabs;

    public float timeBetweenEachSpawn;
    public float timeBetweenRespawn;

    public int numberMaskAtStart;
    public int maxSpawnedMasks;
    int spawnedMasks=0;


    public List<GameObject> spawnPosition = new List<GameObject>();

    float timer = 0f;
	// Use this for initialization
	void Start () {

        List<GameObject> firstMaskGenerate = new List<GameObject>();
        for (int i = 0; i < this.transform.childCount; i++)
        {
            spawnPosition.Add(this.transform.GetChild(i).gameObject);
            firstMaskGenerate.Add(this.transform.GetChild(i).gameObject);
        }
        timer = 0;
        
        for( int i = 0; i < numberMaskAtStart ; i++)
        {
            int random = (int)Random.Range(0, firstMaskGenerate.Count );
            Mask mask = Instantiate(GetRandomMask(),
                firstMaskGenerate[random].transform.position,
                Quaternion.identity ) as Mask;

            
            firstMaskGenerate.RemoveAt(random);
            lastPosition = random;
            spawnedMasks++;
            mask.Spawn(this);
        }

    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if ( timer >= timeBetweenEachSpawn)
        {
            if (spawnedMasks<maxSpawnedMasks) {
                timer = 0;
                Mask mask = Instantiate(GetRandomMask(), GetSpawnPosition().transform.position,
                    Quaternion.identity) as Mask;
                mask.Spawn(this);
                spawnedMasks++;
              
            } else
            {
                timer = timeBetweenEachSpawn- timeBetweenRespawn;
            }
        }
	}

    int lastMask=-1;
    Mask GetRandomMask()
    {
        
        int number = 0;
        if (maskPrefabs.Length > 1)
        {
            number = (int)Random.Range(0, maskPrefabs.Length );
            while (number == lastMask)
            {
                number = (int)Random.Range(0, maskPrefabs.Length );
            }
        }
        lastMask = number;
        return maskPrefabs[number];
    }

    int lastPosition = -1;
    GameObject GetSpawnPosition()
    {

        int number = 0;
        if (spawnPosition.Count > 1)
        {
            number = (int)Random.Range(0, spawnPosition.Count - 1);
            while (number == lastPosition)
            {
                number = (int)Random.Range(0, spawnPosition.Count - 1);
            }
        }
        lastPosition = number;
        return spawnPosition[number];
    }

    public void RegisterDeath(Mask aMask)
    {
        spawnedMasks--;
    }
}
