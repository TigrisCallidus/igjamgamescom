﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class SoundController : MonoBehaviour
{
    private static SoundController curr;
    public static SoundController Current {
        get {
            if (curr == null) curr = GameObject.FindObjectOfType<SoundController>();
            return curr;
        } }

    [Header("Assignments")]
    public AudioSource[] CrowdSources;
    public AudioSource[] PlayerSources;
    public AudioSource[] OtherSources;
    [Space(3f)]
    public AudioSource OrchestraTuningSource;
    public AudioSource OrchestraMusicSource;
    public AudioMixerSnapshot OrchestraTuningSnap;
    public AudioMixerSnapshot OrchestraMusicSnap;

    [Header("Sound-Files")]
    public AudioClip CrowdCheering;
    public AudioClip CrowdClapping;
    public AudioClip CrowdSurprised;

    [Space(5f)]
    public AudioClip[] PlayerDied;
    public AudioClip[] PlayerHit;
    public AudioClip[] PlayerWithoutMask;
    public AudioClip[] PlayerFootsteps;

    [Space(5f)]
    public AudioClip[] GameClick;
    public AudioClip GameCountdown;
    public AudioClip GameJump;

    [Space(5f)]
    public AudioClip SkillShoot;
    public AudioClip SkillSwish;
    public AudioClip BallBounce;
    public AudioClip CurtainOpenClose;
    

    public void PlayCrowdSound(ref AudioClip clip)
    {
        AudioSource source = FindFreeAudioSource(ref CrowdSources);

        if (source != null)
        {
            source.clip = clip;
            source.Play();
        }
    }

    public void PlayPlayerSound(ref AudioClip[] clips, int playerNumber)
    {
        AudioSource source = FindFreeAudioSource(ref PlayerSources);

        if (source != null)
        {
            if (playerNumber < clips.Length)
            {
                source.clip = clips[playerNumber];
                source.Play();
            }
            else
                Debug.LogError("SoundController: Invalid playerNumber: " + playerNumber);
        }
    }

    public void PlayOtherSound(ref AudioClip clip)
    {
        AudioSource source = FindFreeAudioSource(ref OtherSources);

        if (source != null)
        {
            source.clip = clip;
            source.Play();
        }
    }

    public void StopOrchestraTuning()
    {
        OrchestraMusicSource.Stop();
        OrchestraMusicSnap.TransitionTo(4f);
    }
    public void StartMusic()
    {
        OrchestraMusicSource.Play();
    }
    public void StopMusic()
    {
        OrchestraTuningSource.Stop();
        OrchestraTuningSnap.TransitionTo(4f);
    }


    // Helper
    private AudioSource FindFreeAudioSource(ref AudioSource[] sources)
    {
        for (int i = 0; i < sources.Length; i++)
        {
            if (!sources[i].isPlaying)
                return sources[i];
        }
        return null;
    }
}
