﻿using UnityEngine;
using System.Collections;

public class Mask : MonoBehaviour {

    [Header("Assignment")]
    public GameObject DisappearParticlePrefab;

    public int maxHealth;
    public int health;
    public MeshRenderer meshMask;

    public float healthLossInterval = 2; // 2 = lose 1 health every 2 seconds (the smaller the faster health will decrease)
    private float lastHealthLossTime;

    public Skill MaskSkill;
    public Weapon.Element MaskElement;
    public Rigidbody2D UsedBody;
    public Vector3 DropDirection;

    public StrengthModifier[] DamageModifiers;
    public float timeUntilCollision = 0.5f;
    private Player owningPlayer;
    private MaskSpawnController Spawner;

    private Color savedColor;

    public bool IsAvailable
    {
        get
        {
            return owningPlayer == null && !isBroken;
        }
    }

    public int Health
    {
        get
        {
            return health;
        }

        set
        {
            health = value;
            if( meshMask != null )
                meshMask.material.color = Color.Lerp(savedColor, Color.black, (1f - (float)((float)health / (float)maxHealth)));
            if (health <= 0)
            {
                health = 0;
                Break();
            }
            if (health > maxHealth)
            {
                health = maxHealth;
            }
            
        }
    }

    public void Spawn(MaskSpawnController aSpawner)
    {
        Spawner = aSpawner;
    }

    public void DropMask(Player.Direction aDirection)
    {
        owningPlayer = null;

        this.transform.parent = null;
        int sign = 1;
        if (aDirection== Player.Direction.left)
        {
            sign = -1;
        }
        this.UsedBody.gravityScale = 1f;
        this.UsedBody.constraints = RigidbodyConstraints2D.None;
        UsedBody.AddForce(new Vector3(DropDirection.x*sign, DropDirection.y,DropDirection.z)); // Apply force (Flying away)
    }


    public void PickupMask(Player aPlayer)
    {
        this.UsedBody.gravityScale = 0f;
        this.UsedBody.velocity = Vector3.zero;
        this.UsedBody.angularVelocity = 0f;
        this.owningPlayer = aPlayer;
        this.MaskSkill.owningPlayer = aPlayer;

        this.UsedBody.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void UseSkill(Player.Direction aDirection)
    {
        MaskSkill.UseSkill(aDirection);
    }

    public void GotHit(Weapon aWeapon)
    {
        float currentMod = 1;
        for (int i = 0; i < DamageModifiers.Length; i++)
        {
            if (DamageModifiers[i].Element==aWeapon.WeaponElement)
            {
                currentMod = DamageModifiers[i].Modifier;
            }
        }
        this.Health -= Mathf.FloorToInt(currentMod * aWeapon.Damage);
    }

    public bool isBroken = false;



    public void Break()
    {
        isBroken = true;
        StartCoroutine(Disappear());
    }

    IEnumerator Disappear()
    {

        yield return new WaitForSeconds(1f);

        // spawn particle effect
        if (DisappearParticlePrefab != null)
            GameObject.Instantiate(DisappearParticlePrefab, transform.position, Quaternion.identity);
        else
            Debug.LogError("No Disappear PArticle Prefab assigned to mask with name: " + name);
        yield return new WaitForSeconds(0.1f);
        if (Spawner == null)
        {
            Spawner = GameObject.FindObjectOfType(typeof( MaskSpawnController)) as MaskSpawnController;
        }
            Spawner.RegisterDeath(this);
        
        GameObject.Destroy(this.gameObject);
    }

    [System.Serializable]
    public struct StrengthModifier
    {
        public Weapon.Element Element;
        public float Modifier;
    }


    void Start()
    {
        if( meshMask != null)
            savedColor = meshMask.material.color;
        Health = maxHealth;
        UsedBody = GetComponent<Rigidbody2D>();
        Light light = gameObject.AddComponent<Light>();
        light.type = LightType.Point;
        light.color = savedColor;
    }

    void Update()
    {
        if (owningPlayer != null && Time.time - lastHealthLossTime > healthLossInterval)
        {
            Health--;
            lastHealthLossTime = Time.time;
        }
    }
}
