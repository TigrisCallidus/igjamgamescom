﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Player : MonoBehaviour
{
    [Header("Assignment")]
    public Animator mAnimator;
    public SkinnedMeshRenderer ModelRenderer;
    public GameObject StartMaskPrefab;
    public Transform MaskParentBone;

    [Header("Values")]
    public float DeathTimer = 4f;

    [Header("Monitoring")]
    public int PlayerNumber;
    public Mask UsedMask;
    public GameObject HitParticle;


    private bool mAlive;
    public bool IsAlive { get { return mAlive; } }
    public bool mLocked;
    private float mDeathTimerStarted;

    private SpotlightController mSpotlightController;


    // Use this for initialization
    void Start()
    {
        MoveRight();
    }

    // Update is called once per frame
    void Update()
    {
        // Check death timer
        if (mAlive && UsedMask == null && Time.time - mDeathTimerStarted > DeathTimer)
        {
            Die();
        }
        else if(UsedMask == null)
        {
            //mSpotlightController.StartFlashing();
        }

        // check for decrease of health-broken mask
        if (UsedMask != null && UsedMask.isBroken)
        {
            UsedMask.DropMask(currenDirection);
            UsedMask = null;

            // timer starts
            mDeathTimerStarted = Time.time;
        }
    }

    GameManager GameManager;
    public void Initialize(GameManager aGamemManager, int aPlayerNumber, Color aPlayerColor, SpotlightController aSpotlight)
    {
        GameManager = aGamemManager;
        PlayerNumber = aPlayerNumber;
        mSpotlightController = aSpotlight;

        mAlive = true;

        // give basic mask
        Instantiate(StartMaskPrefab, transform.position, Quaternion.identity);

        ModelRenderer.material.color = aPlayerColor;
    }


    public void MoveLeft()
    {
        currenDirection = Direction.left;
    }

    public void MoveRight()
    {
        currenDirection = Direction.right;
    }

    public void Jump()
    {
        if (mAnimator != null)
            mAnimator.SetTrigger("Jump");
    }

    public void Lock()
    {
        mLocked = true;
        GetComponent<MoreMountains.CorgiEngine.CorgiController>().enabled = false;
    }

    public void Unlock()
    {
        mLocked = false;
        GetComponent<MoreMountains.CorgiEngine.CorgiController>().enabled = true;
    }

    public void PickupMask(Mask newMask)
    {
        if (!IsAlive)
        {
            return;
        }
        if (UsedMask != null)
        {
            UsedMask.DropMask(currenDirection);
        }
        else
        {
            mSpotlightController.EndFlashing();
        }
        UsedMask = newMask;
        UsedMask.PickupMask(this);

        UsedMask.transform.parent = MaskParentBone;
        UsedMask.transform.localPosition = Vector3.zero;
        UsedMask.transform.localRotation = Quaternion.identity;
        //GameManager.UpdateMaskState(this);
    }

    public void UseMask()
    {
        if (UsedMask != null && !mLocked)
        {
            mAnimator.SetTrigger("Slash");
            
            UsedMask.UseSkill(currenDirection);
        }
    }

    Direction currenDirection;

    public enum Direction
    {
        left,
        right
    }

    public void GotHit(Weapon aWeapon)
    {
        if (UsedMask != null)
        {
            UsedMask.GotHit(aWeapon);
            //GameManager.UpdateMaskState(this);

            GameObject hitParticle = (GameObject)Instantiate(HitParticle, new Vector3(this.transform.position.x , this.transform.position.y + 0.5f , this.transform.position.z)
                , Quaternion.identity);
            Destroy(hitParticle, 2f); 
            if (UsedMask.isBroken)
            {
                UsedMask.DropMask(currenDirection);
                UsedMask = null;

                // timer starts
                mDeathTimerStarted = Time.time;
                mSpotlightController.StartFlashing();

                SoundController.Current.PlayPlayerSound(ref SoundController.Current.PlayerWithoutMask, PlayerNumber - 1);
            }
            else
            {
                SoundController.Current.PlayPlayerSound(ref SoundController.Current.PlayerHit, PlayerNumber - 1);
            }

            mSpotlightController.DoFlicker();
        }
    }

    private void Die()
    {
        mAlive = false;
        GameManager.RegisterDeath(this);
        mAnimator.SetTrigger("Die");
        SoundController.Current.PlayPlayerSound(ref SoundController.Current.PlayerDied, PlayerNumber - 1);

        mSpotlightController.TurnOff();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Mask aMask = col.GetComponent<Mask>();
        if (aMask != null && aMask.IsAvailable)
        {
            PickupMask(aMask);
        }
        Weapon weapon = col.GetComponent<Weapon>();
        if (weapon != null && weapon.owningPlayer != this)
        {
            StartVibration(0.1f);
            this.GetComponent<MoreMountains.CorgiEngine.CorgiController>().AddVerticalForce(4f);
        }
    }

    public void StartVibration(float delay = 0f)
    {
        StartCoroutine(Vibration(delay));
    }

    private PlayerIndex getPlayerIndex()
    {
        if (PlayerNumber == 1)
            return PlayerIndex.One;
        else if (PlayerNumber == 2)
            return PlayerIndex.Two;
        else if (PlayerNumber == 3)
            return PlayerIndex.Three;
        else if (PlayerNumber == 4)
            return PlayerIndex.Four;

        return PlayerIndex.One;
    }


    IEnumerator Vibration(float delay)
    {
        yield return new WaitForSeconds(delay);

        GamePad.SetVibration(getPlayerIndex(), 1f, 1f);
        yield return new WaitForSeconds(0.1f);
        GamePad.SetVibration(getPlayerIndex(), 0f, 0f);
    }
}
