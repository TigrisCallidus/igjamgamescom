﻿using UnityEngine;
using System.Collections;

public class TestManager : MonoBehaviour {

    Player testPlayer;

	// Use this for initialization
	void Start () {
        testPlayer = GameObject.FindObjectOfType<Player>();

        if (testPlayer == null) Debug.LogError("No Player found!");
	}

    public void OnPlayerUseWeapon()
    {
        Debug.Log("Player Used Weapon!");    
    }

}
