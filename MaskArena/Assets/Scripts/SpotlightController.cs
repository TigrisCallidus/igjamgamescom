﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class SpotlightController : MonoBehaviour
{

    private Transform mFollowTarget;
    private Light mLight;
    private bool mFlashing = false;
    private float mInitialIntensity;

	public void Initialize(Transform aFollowTarget, Color aLightColor)
    {
        this.mFollowTarget = aFollowTarget;

        mLight = GetComponent<Light>();
        mInitialIntensity = mLight.intensity;
        mLight.intensity = 0f;

        // Tween intensity to finalIntensity in 1sec
        TurnOn(mInitialIntensity);

        mLight.color = aLightColor;
    }

    public void StartFlashing()
    {
        mFlashing = true;
    }
    public void EndFlashing()
    {
        mFlashing = false;
        TurnOn(mInitialIntensity);
    }

    public void TurnOff()
    {
        if (mFlashing)
            EndFlashing();

        LeanTween.value(gameObject, mLight.intensity, 0f, 1f).setOnUpdate((float val) => {
            mLight.intensity = val;
        }).setEase(LeanTweenType.easeInOutCubic);
    }
    public void TurnOn(float toIntensity = 1f)
    {
        if (mFlashing)
            EndFlashing();

        LeanTween.value(gameObject, mLight.intensity, toIntensity, 1f).setOnUpdate((float val) => {
            mLight.intensity = val;
        }).setEase(LeanTweenType.easeInOutCubic);
    }

    public void DoFlicker()
    {
        StartCoroutine(Flickering());
    }
    IEnumerator Flickering()
    {
        for (int i = 0; i < 30; i++)
        {
            mLight.intensity = Random.Range(0.2f, mInitialIntensity * 2f);
            yield return new WaitForSeconds(0.02f);
        }
        mLight.intensity = mInitialIntensity;
    }

    bool lightDown = true;
    void FixedUpdate()
    {
        if (mLight != null)
        {
            transform.LookAt(mFollowTarget);

            if (mFlashing)
            {
                mLight.intensity += (lightDown ? -Time.fixedDeltaTime * 5f : Time.fixedDeltaTime * 5f);
                if (lightDown && mLight.intensity < 0.01f)
                    lightDown = false;
                else if (!lightDown && mLight.intensity > mInitialIntensity - 0.01f)
                    lightDown = true;
            }
        }
    }
}
