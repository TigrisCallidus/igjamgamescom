﻿using UnityEngine;
using System.Collections;

public class Skill : MonoBehaviour {


    public Player owningPlayer;

    public Weapon[] SpawnedWeapon;
    int currentPos = 0;
    public float Cooldown=1;
    float LastAttack=0;

    public void UseSkill(Player.Direction aDirection)
    {
        if (LastAttack <= 0)
        {
            SpawnedWeapon[currentPos].owningPlayer = this.owningPlayer;
            SpawnedWeapon[currentPos].Reset(this.transform);
            SpawnedWeapon[currentPos].Attack(aDirection);
            
            LastAttack = Cooldown;
            currentPos = (currentPos + 1) % SpawnedWeapon.Length;
        }

    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (LastAttack > 0)
        {
            LastAttack = LastAttack - Time.deltaTime;
        }
	}
}
