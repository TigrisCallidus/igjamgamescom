﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

    public Player owningPlayer;

    public int Damage;
    public float AttackDuration;
    public Element WeaponElement;
    public bool isShot = true;
    public Vector3 AppliedForce;
    public Vector3 EndPosition;
    public Vector3 EndRotation;
    Vector3 StartPosition;
    Vector3 StartRotation;
    float movingDuration = 0;
    Rigidbody2D Body;
    int sign = 1;
    public Element[] DestroyableBy;
    bool destroyed = false;
    public MeshRenderer Model;

    public virtual void Attack(Player.Direction aDirection)
    {
        if (aDirection == Player.Direction.left)
        {
            sign = -1;
        } else
        {
            sign = 1;
        }

        this.gameObject.SetActive(true);
        if (isShot)
        {
            Body.AddForce(new Vector3( AppliedForce.x*sign, AppliedForce.y, AppliedForce.z));

            SoundController.Current.PlayOtherSound(ref SoundController.Current.SkillShoot);
        }
        else
        {
            this.EndPosition.x = this.EndPosition.x * sign;
            this.EndRotation.z = this.EndRotation.z * sign;
            this.StartPosition = this.transform.localPosition;
            this.StartRotation = this.transform.rotation.eulerAngles;
            movingDuration = AttackDuration;

            SoundController.Current.PlayOtherSound(ref SoundController.Current.SkillSwish);
        }
        if (Model == null)
        {
            Model = GetComponent<MeshRenderer>();
        }
        Model.material.color = owningPlayer.ModelRenderer.material.color;
        this.transform.localScale= new Vector3( sign,this.transform.localScale.y, this.transform.localScale.y);
        
        //Movement
        StartCoroutine(AttackFinished(AttackDuration));
    }

    IEnumerator AttackFinished(float aDuration)
    {
        yield return new WaitForSecondsRealtime(aDuration);
        if (!destroyed)
        {
            Destroy();
        }
    }

    public void Destroy()
    {
        if (!destroyed)
        {
            // Maybe Play Particle effect
            movingDuration = 0;
            this.gameObject.SetActive(false);
            this.EndPosition.x = this.EndPosition.x * sign;
            this.EndRotation.z = this.EndRotation.z * sign;
            destroyed = true;
        }
    }

    public void Reset(Transform aTransform)
    {
        destroyed = false;
        if (Body == null)
        {
            this.Body = this.gameObject.GetComponent<Rigidbody2D>();
        }
        this.Body.velocity = Vector3.zero;
        this.transform.position = aTransform.transform.position;
        this.transform.rotation = Quaternion.identity;
        /*
        this.transform.rotation = new Quaternion(aTransform.transform.rotation.x, 
            aTransform.transform.rotation.z, aTransform.transform.rotation.z, aTransform.transform.rotation.w) ;
            */
        if (!isShot)
        {
            this.transform.parent = aTransform;
        } else
        {
            this.transform.parent = null;
        }
        deathCounter = float.MaxValue;
    }

    public enum Element
    {
        Sword,
        Staff,
        Rolling,
        Bouncing,
        Shot
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        Player aPlayer = col.GetComponent<Player>();
        if (aPlayer != null && this.owningPlayer != aPlayer)
        {
            aPlayer.GotHit(this);
        }

        Weapon aWeapon = col.GetComponent<Weapon>();
        if (aWeapon != null)
        {
            for (int i = 0; i < DestroyableBy.Length; i++)
            {
                if (DestroyableBy[i]==aWeapon.WeaponElement)
                {
                    Destroy();
                    break;
                }
            }
        }


    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (WeaponElement == Element.Bouncing)
            SoundController.Current.PlayOtherSound(ref SoundController.Current.BallBounce);
    }
    public float maxDeathTime = 0.2f;
    float deathCounter=float.MaxValue;
    void Update()
    {
        if (movingDuration>0)
        {
            this.transform.position = this.transform.parent.position+ StartPosition * (movingDuration) / AttackDuration 
                + EndPosition*(AttackDuration-movingDuration) /AttackDuration;
            Quaternion quat = new Quaternion();
            quat.eulerAngles= StartRotation * (movingDuration) / AttackDuration
                + EndRotation * (AttackDuration - movingDuration) / AttackDuration;
            this.transform.rotation = quat;                
            movingDuration -= Time.deltaTime;
        } else
        {
            if (this.Body.velocity.magnitude <= 0.1f)
            {
                if (Time.time - deathCounter > maxDeathTime)
                {
                    Destroy();
                }
                else
                {
                    deathCounter =Mathf.Min( Time.time, deathCounter);
                }
            } else
            {
                deathCounter = float.MaxValue;
            }
        }
        
    }
}
