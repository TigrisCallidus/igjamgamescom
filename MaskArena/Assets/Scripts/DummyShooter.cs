﻿using UnityEngine;
using System.Collections;

public class DummyShooter : MonoBehaviour {

    public Skill UsedSkill;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            UsedSkill.UseSkill(Player.Direction.left);
        }
	}
}
