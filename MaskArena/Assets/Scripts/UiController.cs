﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class UiController : MonoBehaviour {

    [Header("Assignments")]
    public RectTransform MaskEffectLogo;
    public Text AnnounceText;
    public RectTransform CurtainLeft;
    public RectTransform CurtainRight;
    public RectTransform Audience;
    public Image Credits;
    public Selectable BtnStart;
    public Selectable BtnCredits;

    [Space(5f)]
    public UiPlayerElement[] PlayerUIs= new UiPlayerElement[4];

	// Use this for initialization
	void Start () {
        ShowMenu();
        ShowAudience();
	}


    public void StartGame()
    {
        OpenCurtains();
        HideAudience();
        HideMenu();
    }

    public void StartCountDown(int timer)
    {
        AnnounceText.text = timer.ToString();
        AnnounceText.gameObject.SetActive(true);
        StartCoroutine(Countdown(timer));
    }
    IEnumerator Countdown(int timer)
    {
        while (timer > 0)
        {
            yield return new WaitForSecondsRealtime(1f);
            AnnounceText.text = (--timer).ToString();
        }
        AnnounceText.text = "GO!";
        yield return new WaitForSecondsRealtime(1f);
        AnnounceText.gameObject.SetActive(false);
    }

    public void AnnounceWinner(Player aPlayer)
    {
        AnnounceText.text = "Player " + (aPlayer.PlayerNumber) + " won!";
        AnnounceText.gameObject.SetActive(true);

        StartCoroutine(CloseCurtains(5f));
    }

    public void UpdateMaskState(Player aPlayer)
    {
        PlayerUIs[aPlayer.PlayerNumber].UpdateMask(aPlayer.UsedMask);
    }

    public void OpenCurtains()
    {
        LeanTween.value(CurtainLeft.gameObject,
            CurtainLeft.anchoredPosition.x,
            CurtainLeft.anchoredPosition.x - CurtainLeft.sizeDelta.x,
            3f).setOnUpdate((float val) => {
                CurtainLeft.anchoredPosition = new Vector2(val, CurtainLeft.anchoredPosition.y);
            }).setEase(LeanTweenType.easeInOutCubic);

        LeanTween.value(CurtainRight.gameObject,
            CurtainRight.anchoredPosition.x,
            CurtainRight.anchoredPosition.x + CurtainLeft.sizeDelta.x,
            3f).setOnUpdate((float val) => {
                CurtainRight.anchoredPosition = new Vector2(val, CurtainRight.anchoredPosition.y);
            }).setEase(LeanTweenType.easeInOutCubic);

        SoundController.Current.PlayOtherSound(ref SoundController.Current.CurtainOpenClose);
    }
    public void CloseCurtains()
    {
        LeanTween.value(CurtainLeft.gameObject,
            CurtainLeft.anchoredPosition.x,
            CurtainLeft.anchoredPosition.x + CurtainLeft.sizeDelta.x,
            3f).setOnUpdate((float val) => {
                CurtainLeft.anchoredPosition = new Vector2(val, CurtainLeft.anchoredPosition.y);
            }).setEase(LeanTweenType.easeInOutCubic);

        LeanTween.value(CurtainRight.gameObject,
            CurtainRight.anchoredPosition.x,
            CurtainRight.anchoredPosition.x - CurtainLeft.sizeDelta.x,
            3f).setOnUpdate((float val) => {
                CurtainRight.anchoredPosition = new Vector2(val, CurtainRight.anchoredPosition.y);
            }).setEase(LeanTweenType.easeInOutCubic);

        SoundController.Current.PlayOtherSound(ref SoundController.Current.CurtainOpenClose);
    }

    public void HideAudience()
    {
        LeanTween.cancel(Audience.gameObject);
        LeanTween.value(Audience.gameObject,
           Audience.anchoredPosition.y,
           Audience.anchoredPosition.y - 100f,
           1f).setOnUpdate((float val) => {
               Audience.anchoredPosition = new Vector2(Audience.anchoredPosition.x, val);
           }).setEase(LeanTweenType.easeInQuad);
    }
    public void ShowAudience()
    {
        LeanTween.cancel(Audience.gameObject);
        LeanTween.value(Audience.gameObject,
           Audience.anchoredPosition.y,
           Audience.anchoredPosition.y + 100f,
           2f).setOnUpdate((float val) => {
               Audience.anchoredPosition = new Vector2(Audience.anchoredPosition.x, val);
           }).setEase(LeanTweenType.easeOutQuad);
    }

    public void HideMenu()
    {
        LeanTween.cancel(MaskEffectLogo.gameObject);
        LeanTween.value(MaskEffectLogo.gameObject,
           MaskEffectLogo.anchoredPosition.y,
           MaskEffectLogo.anchoredPosition.y + 150f,
           0.8f).setOnUpdate((float val) => {
               MaskEffectLogo.anchoredPosition = new Vector2(MaskEffectLogo.anchoredPosition.x, val);
           }).setEase(LeanTweenType.easeInQuad);

        LeanTween.cancel(BtnStart.gameObject);
        LeanTween.value(BtnStart.gameObject,
          BtnStart.GetComponent<RectTransform>().anchoredPosition.y,
          BtnStart.GetComponent<RectTransform>().anchoredPosition.y + 300f,
          0.8f).setOnUpdate((float val) => {
              BtnStart.GetComponent<RectTransform>().anchoredPosition = new Vector2(BtnStart.GetComponent<RectTransform>().anchoredPosition.x, val);
              BtnCredits.GetComponent<RectTransform>().anchoredPosition = new Vector2(BtnCredits.GetComponent<RectTransform>().anchoredPosition.x, val);
          }).setEase(LeanTweenType.easeInBounce);
    }
    public void ShowMenu()
    {
        LeanTween.cancel(MaskEffectLogo.gameObject);
        LeanTween.value(MaskEffectLogo.gameObject,
           MaskEffectLogo.anchoredPosition.y,
           MaskEffectLogo.anchoredPosition.y - 200f,
           1f).setOnUpdate((float val) => {
               MaskEffectLogo.anchoredPosition = new Vector2(MaskEffectLogo.anchoredPosition.x, val);
           }).setEase(LeanTweenType.easeOutQuad);

        LeanTween.cancel(BtnStart.gameObject);
        LeanTween.value(BtnStart.gameObject,
          BtnStart.GetComponent<RectTransform>().anchoredPosition.y,
          BtnStart.GetComponent<RectTransform>().anchoredPosition.y - 350f,
          0.7f).setDelay(0.5f).setOnUpdate((float val) =>
          {
              BtnStart.GetComponent<RectTransform>().anchoredPosition = new Vector2(BtnStart.GetComponent<RectTransform>().anchoredPosition.x, val);
              BtnCredits.GetComponent<RectTransform>().anchoredPosition = new Vector2(BtnCredits.GetComponent<RectTransform>().anchoredPosition.x, val);
          }).setEase(LeanTweenType.easeOutBounce);
    }

    public void ShowCredits()
    {
        LeanTween.cancel(Credits.gameObject);
        LeanTween.value(Credits.gameObject,
          0f,
          180f / 255f,
          1f).setOnUpdate((float val) =>
          {
              Credits.color = new Color(1f, 1f, 1f, val);
          }).setEase(LeanTweenType.easeOutQuad);

        Credits.raycastTarget = true;
    }
    public void HideCredits()
    {
        LeanTween.cancel(Credits.gameObject);
        LeanTween.value(Credits.gameObject,
          180f / 255f,
          0f,
          1f).setOnUpdate((float val) =>
          {
              Credits.color = new Color(1f, 1f, 1f, val);
          }).setEase(LeanTweenType.easeInQuad);

        Credits.raycastTarget = false;
    }

    IEnumerator CloseCurtains(float delaySecs)
    {
        yield return new WaitForSeconds(delaySecs);
        CloseCurtains();
    }
}
