﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameManager : MonoBehaviour {

    [Header("Assignments")]
    public GameObject PlayerPrefab;
    public GameObject SpotlightPrefab;
    public GameObject PS_PlayerAppear;
    public string SceneName;

    [Header("Sounds")]
    public AudioClip WinCheer;

    [Header("Values")]
    public Color[] PlayerColors;

    public List<Player> spawnedPLayers = new List<Player>();

    [Space(10f)]
    private int numberOfPlayers=2;
    private int alivePlayers;

    public UiController UI;

    public Mask[] PlacedMasks = new Mask[20];
    int currentMasks = 0;

    public int StartingCountDown = 3;

    enum GameState { InMenu, Start, Ingame, End }
    private GameState currState;

    // Use this for initialization
    void Start () {
        currState = GameState.InMenu;
    }

    IEnumerator StartGame()
    {
        currState = GameState.Start;

        SoundController.Current.StopOrchestraTuning();

        SceneManager.LoadScene(SceneName, LoadSceneMode.Additive);

        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();

        SoundController.Current.PlayCrowdSound(ref SoundController.Current.CrowdClapping);

        UI.StartGame();

        string[] devices = Input.GetJoystickNames();
        int countDevices = 0;
        Debug.Log("## List of Input-Devices ##");
        for (int i = 0; i < devices.Length; i++)
        {
            if (devices[i] != string.Empty) countDevices++;
            Debug.Log("P" + (i + 1) + ": " + devices[i]);
        }
        numberOfPlayers = (countDevices == 0 ? 1 : countDevices);
        alivePlayers = numberOfPlayers;

        List<GameObject> spawnPoints = new List<GameObject>(GameObject.FindGameObjectsWithTag("spawnpoint"));

        if (spawnPoints.Count < numberOfPlayers) Debug.LogError("Need at least as many Spawnoints as players!");

        yield return new WaitForSeconds(2f);
        for (int i = 0; i < numberOfPlayers; i++)
        {
            // choose random spawnpoint
            int random = UnityEngine.Random.Range(0, spawnPoints.Count);
            GameObject spawnPoint = spawnPoints[random]; spawnPoints.RemoveAt(random);

            GameObject ps = (GameObject)GameObject.Instantiate(PS_PlayerAppear, spawnPoint.transform.position, Quaternion.identity);
            ps.GetComponent<ParticleSystem>().startColor = PlayerColors[i];

            yield return new WaitForSeconds(0.5f);

            GameObject spawnedPlayer = (GameObject)
                MoreMountains.CorgiEngine.GameManager.Instantiate(
                    PlayerPrefab,
                    spawnPoint.transform.position, 
                    Quaternion.identity);


            Player tempPlayer = spawnedPlayer.GetComponent<Player>();
            spawnedPLayers.Add(tempPlayer);

            GameObject spawnedSpotlight = (GameObject)GameObject.Instantiate(SpotlightPrefab);
            spawnedSpotlight.GetComponent<SpotlightController>().Initialize(spawnedPlayer.transform, PlayerColors[i]);

            tempPlayer.Initialize(this, i + 1, PlayerColors[i], spawnedSpotlight.GetComponent<SpotlightController>());
            tempPlayer.Lock();
            tempPlayer.StartVibration();

            yield return new WaitForSeconds(0.5f);
        }

        MoreMountains.CorgiEngine.GameManager.Instance.Player = new MoreMountains.CorgiEngine.CharacterBehavior[spawnedPLayers.Count];

        spawnedPLayers.Reverse();
        for (int i = 0; i < spawnedPLayers.Count; i++)
        {
            Debug.Log(spawnedPLayers[i].PlayerNumber);
            MoreMountains.CorgiEngine.GameManager.Instance.Player[i] =
                spawnedPLayers[i].GetComponent<MoreMountains.CorgiEngine.CharacterBehavior>();
        }
        MoreMountains.CorgiEngine.InputManager.Instance.SetPlayers();

        UI.StartCountDown(StartingCountDown);
        SoundController.Current.PlayOtherSound(ref SoundController.Current.GameCountdown);
        yield return new WaitForSeconds(StartingCountDown);

        SoundController.Current.StartMusic();

        foreach (var player in spawnedPLayers)
        {
            player.Unlock();
        }
        currState = GameState.Ingame;
    }

	// Update is called once per frame
	void Update () {

        if (currState == GameState.InMenu && Input.GetButtonDown("Start"))
            StartCoroutine(StartGame());  
    }

    public void DoStartGame()
    {
        if (currState == GameState.InMenu)
            StartCoroutine(StartGame());
    }

    public void UpdateMaskState(Player aPlayer)
    {
        UI.UpdateMaskState(aPlayer);
    }

    public void RegisterDeath(Player aPlayer)
    {
        if (currState == GameState.Ingame)
        {
            alivePlayers--;
            aPlayer.Lock();
            if (alivePlayers == 1)
            {
                Player[] players = spawnedPLayers.ToArray();
                Player winningPlayer = players[0];
                for (int i = 0; i < players.Length; i++)
                    if (players[i].IsAlive)
                    {
                        winningPlayer = players[i];
                        break;
                    }
                AnnounceWinner(winningPlayer);
            }
            else
            {
                SoundController.Current.PlayCrowdSound(ref SoundController.Current.CrowdSurprised);
            }
        }
    }

    void AnnounceWinner(Player aPlayer)
    {

        GameObject ps = (GameObject)GameObject.Instantiate(PS_PlayerAppear, aPlayer.transform.position, Quaternion.identity);
        ps.GetComponent<ParticleSystem>().startColor = PlayerColors[aPlayer.PlayerNumber-1];
        Destroy(ps, 2f);

        aPlayer.transform.position = new Vector3(3.50f, 0.85f, 0);
        aPlayer.transform.Rotate(new Vector3(0f, 90f, 0f));

        GameObject psAfter = (GameObject)GameObject.Instantiate(PS_PlayerAppear, aPlayer.transform.position, Quaternion.identity);
        psAfter.GetComponent<ParticleSystem>().startColor = PlayerColors[aPlayer.PlayerNumber - 1];
        Destroy(psAfter, 2f);

        currState = GameState.End;

        aPlayer.Lock();
        UI.AnnounceWinner(aPlayer);

        SoundController.Current.StopMusic();
        SoundController.Current.PlayCrowdSound(ref SoundController.Current.CrowdCheering);

        StartCoroutine(RestartAfterSeconds(8f));

        /* Zoom camera in */
        Vector3 finalRot =
            Quaternion.LookRotation(aPlayer.transform.position - Camera.main.transform.position).eulerAngles;

        LeanTween.rotate(Camera.main.gameObject, finalRot, 2f).setEase(LeanTweenType.easeInOutQuad);
        LeanTween.value(Camera.main.gameObject,
            Camera.main.fieldOfView,
            15f,
            2f).setOnUpdate((float val) => {
                Camera.main.fieldOfView = val;
            }).setEase(LeanTweenType.easeInOutQuad);

        /* Bring player up close
        LeanTween.move(aPlayer.gameObject, new Vector3(0f, 6f, -7f), 1f);
        LeanTween.rotateAround(aPlayer.gameObject, Vector3.up, 90f, 1f);*/
    }

    IEnumerator RestartAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        GameObject destroyGM = GameObject.FindObjectOfType<MoreMountains.CorgiEngine.GameManager>().gameObject;
        if (destroyGM != null) GameObject.Destroy(destroyGM);

        MoreMountains.CorgiEngine.InputManager.Instance.SetPlayers();

        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
