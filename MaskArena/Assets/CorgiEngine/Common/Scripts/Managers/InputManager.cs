using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using XInputDotNetPure;

namespace MoreMountains.CorgiEngine
{	
	/// <summary>
	/// This persistent singleton handles the inputs and sends commands to the player
	/// </summary>
	public class InputManager : Singleton<InputManager>
	{
		public enum InputForcedMode { None, Mobile, Desktop }
		[Information("If you check Auto Mobile Detection, the engine will automatically switch to mobile controls when your build target is Android or iOS. You can also force mobile or desktop (keyboard, gamepad) controls using the dropdown below.\nNote that if you don't need mobile controls and/or GUI this component can also work on its own, just put it on an empty GameObject instead.",InformationAttribute.InformationType.Info,false)]
		public bool AutoMobileDetection = true;
		public InputForcedMode ForcedMode;
		public bool IsMobile { get; protected set; }
		[HideInInspector]
		public bool InDialogueZone;
	    protected static CharacterBehavior[] _player;
	    protected static CorgiController[] _controller;

	    protected float _horizontalMove = 0;
	    protected float _verticalMove = 0;

	    /// <summary>
	    /// We get the player from its tag.
	    /// </summary>
	    protected virtual void Start()
		{
			InDialogueZone = false;

			if (GUIManager.Instance!=null)
			{
				GUIManager.Instance.SetMobileControlsActive(false);
				IsMobile=false;
				if (ForcedMode==InputForcedMode.Mobile)
				{
					GUIManager.Instance.SetMobileControlsActive(true);
					IsMobile = true;
				}
				if (ForcedMode==InputForcedMode.Desktop)
				{
					GUIManager.Instance.SetMobileControlsActive(false);
					IsMobile = false;					
				}
			}
		}

        public void SetPlayers()
        {
            if (GameManager.Instance.Player != null)
            {
                _player = GameManager.Instance.Player;
                _controller = new CorgiController[_player.Length];
                for (int i = 0; i < _player.Length; i++)
                {
                    if (_player[i].GetComponent<CorgiController>() != null)
                    {
                        _controller[i] = _player[i].GetComponent<CorgiController>();
                    }
                }
            }
        }

	    /// <summary>
	    /// At update, we check the various commands and send them to the player.
	    /// </summary>
	    protected virtual void Update()
		{		

			// if we can't get the player, we do nothing
			if (_player == null) 
			{
				if (GameManager.Instance.Player!=null)
				{
                    _player = GameManager.Instance.Player;
                    _controller = new CorgiController[_player.Length];
                    for (int i = 0; i < _player.Length; i++)
                    {
                        if ( _player[i].GetComponent<CorgiController>() != null)
                        {
                            _controller[i] = _player[i].GetComponent<CorgiController>();
                        }
                    }
                }
				else
				{
					return;
				}
			}
			
			if ( Input.GetButtonDown("Pause"))
			{
				Pause();
			}
				
			if (GameManager.Instance.Paused)
			{
				return;	
			}

            
            for (int i = 0; i < _player.Length; i++)
            {
                
                CheckInputByJoystick(i+ 1 );
            }


        }

        private PlayerIndex getPlayerIndex(int PlayerNumber)
        {
            if (PlayerNumber == 1)
                return PlayerIndex.One;
            else if (PlayerNumber == 2)
                return PlayerIndex.Two;
            else if (PlayerNumber == 3)
                return PlayerIndex.Three;
            else if (PlayerNumber == 4)
                return PlayerIndex.Four;

            return PlayerIndex.One;
        }


        void CheckInputByJoystick(int playerNumber)
        {
            if (_player[playerNumber - 1] == null) return;

            GamePadState state = GamePad.GetState(getPlayerIndex(_player[playerNumber - 1].GetComponent<Player>().PlayerNumber ));

           // playerNumber = _player[playerNumber - 1].GetComponent<Player>().PlayerNumber;

            SetMovement(playerNumber,state);

        
          
            if (state.Buttons.A == ButtonState.Pressed) 
            {
                JumpPressedDown(playerNumber);
            }
            /*
            if (Input.GetButtonUp("Jump" + playerNumber.ToString()))
            {
                JumpReleased(playerNumber);
            }*/


            if (state.Buttons.X == ButtonState.Pressed)
            {
                ShootOnce(playerNumber);
            }
            /*
            if (Input.GetButton("Fire" + playerNumber.ToString()))
            {

                ShootStart(playerNumber);
            }

            if (Input.GetButtonUp("Fire" + playerNumber.ToString()))
            {
                ShootStop(playerNumber);
            }*/



        }


		public virtual void SetMovement(int numberPlayer, GamePadState state)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
            if (!IsMobile && _player[numberPlayer - 1] != null)
            {
                //Debug.Log("Horizontal" + numberPlayer.ToString());
                //Debug.Log(Input.GetAxis("Horizontal" + numberPlayer.ToString()));
                _player[numberPlayer-1].SetHorizontalMove(state.ThumbSticks.Left.X);
                if (_player[numberPlayer - 1] != null && _player[numberPlayer - 1].GetComponent<Player>() != null)
                {
                    if (state.ThumbSticks.Left.X < 0)
                        _player[numberPlayer - 1].GetComponent<Player>().MoveLeft();
                    else if (state.ThumbSticks.Left.X > 0)
                        _player[numberPlayer - 1].GetComponent<Player>().MoveRight();


                    _player[numberPlayer - 1].SetVerticalMove(state.ThumbSticks.Left.Y);
                    if (_player[numberPlayer - 1] != null && _player[numberPlayer - 1].GetComponent<CharacterShoot>() != null)
                    {
                        _player[numberPlayer - 1].GetComponent<CharacterShoot>().SetHorizontalMove(state.ThumbSticks.Left.X);
                        _player[numberPlayer - 1].GetComponent<CharacterShoot>().SetVerticalMove(state.ThumbSticks.Left.Y);
                    }
                }
			}
		}

		public virtual void SetMovement(Vector2 movement, int numberPlayer)
		{
			MMEventManager.TriggerFloatEvent("HorizontalMovement",movement.x);
			MMEventManager.TriggerFloatEvent("VerticalMovement",movement.y);
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			if (IsMobile)
			{
				_player[numberPlayer-1].SetHorizontalMove(movement.x);
				_player[numberPlayer-1].SetVerticalMove(movement.y);	
				if (_player[numberPlayer-1].GetComponent<CharacterShoot>()!=null)
				{
					_player[numberPlayer-1].GetComponent<CharacterShoot>().SetHorizontalMove(movement.x);
					_player[numberPlayer-1].GetComponent<CharacterShoot>().SetVerticalMove(movement.y);
				}
			}
		}	

		public virtual void JumpPressedDown(int numberPlayer)
		{
			MMEventManager.TriggerEvent("Jump");

			if (_player==null || _player[numberPlayer-1] == null) { return; }

			// if the player is in a dialogue zone, we handle it
			if ((_player[numberPlayer-1].BehaviorState.InButtonActivatedZone)
			    &&(_player[numberPlayer-1].BehaviorState.ButtonActivatedZone!=null)
			    &&(!_player[numberPlayer-1].BehaviorState.IsDead)
			    &&(!_player[numberPlayer-1].BehaviorState.Firing)
			    &&(!_player[numberPlayer-1].BehaviorState.Dashing))
			{
				_player[numberPlayer-1].BehaviorState.ButtonActivatedZone.TriggerButtonAction();
			}
			
			if (!_player[numberPlayer-1].BehaviorState.InButtonActivatedZone)
			{
				_player[numberPlayer-1].JumpStart ();
                _player[numberPlayer-1].GetComponent<Player>().Jump();
            }
		}

		public virtual void JumpReleased(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			_player[numberPlayer-1].JumpStop();
		}

		public virtual void Dash(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			_player[numberPlayer].Dash();
		}

		public virtual void Run(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			_player[numberPlayer-1].RunStart();		
		}

		public virtual void RunStop(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			_player[numberPlayer-1].RunStop();	
		}

		public virtual void Jetpack(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			if (_player[numberPlayer-1].GetComponent<CharacterJetpack>()!=null)
			{
				_player[numberPlayer-1].GetComponent<CharacterJetpack>().JetpackStart();
			}
		}

		public virtual void JetpackStop(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			if (_player[numberPlayer-1].GetComponent<CharacterJetpack>()!=null)
			{
				_player[numberPlayer-1].GetComponent<CharacterJetpack>().JetpackStop();
			}
		}

		public virtual void Melee(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			if (_player[numberPlayer-1].GetComponent<CharacterMelee>() != null) 
			{				
				_player[numberPlayer-1].GetComponent<CharacterMelee>().Melee();
			}
		}

		public virtual void ShootOnce(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null || _player[numberPlayer-1] == null) { return; }
			if (_player[numberPlayer-1].GetComponent<Player>() != null) 
			{
				_player[numberPlayer-1].GetComponent<Player>().UseMask();		
			}
		}

		public virtual void ShootStart(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			if (_player[numberPlayer-1] != null && _player[numberPlayer-1].GetComponent<CharacterShoot>() != null) 
			{
				_player[numberPlayer-1].GetComponent<CharacterShoot>().ShootStart();
			}
		}

		public virtual void ShootStop(int numberPlayer)
		{
			if (!GameManager.Instance.CanMove || _player==null) { return; }
			if (_player[numberPlayer-1] != null && _player[numberPlayer-1].GetComponent<CharacterShoot>() != null) 
			{
				_player[numberPlayer-1].GetComponent<CharacterShoot>().ShootStop();
			}
		}

		public virtual void Pause()
		{
			GameManager.Instance.Pause();
		}
	}
}